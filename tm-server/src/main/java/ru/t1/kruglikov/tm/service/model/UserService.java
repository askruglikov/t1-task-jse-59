package ru.t1.kruglikov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.kruglikov.tm.api.repository.model.IProjectRepository;
import ru.t1.kruglikov.tm.api.repository.model.ISessionRepository;
import ru.t1.kruglikov.tm.api.repository.model.ITaskRepository;
import ru.t1.kruglikov.tm.api.repository.model.IUserRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.api.service.model.IUserService;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.enumerated.UserSort;
import ru.t1.kruglikov.tm.exception.entity.*;
import ru.t1.kruglikov.tm.exception.field.*;
import ru.t1.kruglikov.tm.exception.user.*;
import ru.t1.kruglikov.tm.model.User;
import ru.t1.kruglikov.tm.repository.model.ProjectRepository;
import ru.t1.kruglikov.tm.repository.model.SessionRepository;
import ru.t1.kruglikov.tm.repository.model.TaskRepository;
import ru.t1.kruglikov.tm.repository.model.UserRepository;
import ru.t1.kruglikov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Service
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public List<User> findAll(@Nullable UserSort sort) {
            if (sort == null) return repository.findAll();
            return repository.findAll(sort);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @Transactional
    public void removeAll() {
        @Nullable final List<User> users = findAll();
        if (users == null) return;
        for (final User user : users) {
            removeOne(user);
        }
    }

    @Nullable
    @Override
    @Transactional
    public User removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @Nullable final User user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @Nullable
    @Override
    @Transactional
    public User removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @Nullable final User user = findByEmail(email);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Transactional
    private User update(@Nullable final User user) {
        if (user == null) throw new ProjectNotFoundException();
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));

        return update(user);
    }

    @NotNull
    @Override
    @Transactional
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final User user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        return update(user);
    }

    @NotNull
    @Transactional
    private User setLockOneByLogin(
            @Nullable final String login,
            @NotNull final Boolean locked
    ) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(locked);

        return update(user);
    }

    @NotNull
    @Override
    @Transactional
    public User lockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, true);
    }

    @NotNull
    @Override
    @Transactional
    public User unlockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, false);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

}
