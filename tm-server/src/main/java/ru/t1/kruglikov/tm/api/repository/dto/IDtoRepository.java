package ru.t1.kruglikov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void update(@NotNull M model);

    void remove(@NotNull M model);

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    void removeOneById(@NotNull String id);

    void removeAll();

    long getSize();

}


