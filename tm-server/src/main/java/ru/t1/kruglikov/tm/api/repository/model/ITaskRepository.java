package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.enumerated.TaskSort;
import ru.t1.kruglikov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findAll(
            @NotNull String userId,
            @NotNull TaskSort sort
    );

    @Nullable
    List<Task> findAll(
            @NotNull TaskSort sort
    );

    @Nullable
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeTasksByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeAll();

    void removeAll(@NotNull String userId);

}