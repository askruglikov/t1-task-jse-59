package ru.t1.kruglikov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull private final String NAME = "user-logout";
    @NotNull private final String DESCRIPTION = "Log out.";

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOGOUT]");

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        authEndpoint.logout(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
