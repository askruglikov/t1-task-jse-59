package ru.t1.kruglikov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.api.repository.dto.IDtoRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.dto.IDtoService;
import ru.t1.kruglikov.tm.dto.model.AbstractModelDTO;
import ru.t1.kruglikov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.ValueIsNullException;
import ru.t1.kruglikov.tm.exception.field.IdEmptyException;
import ru.t1.kruglikov.tm.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected IConnectionService connectionService;

    public AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Autowired
    protected IDtoRepository<M> repository;

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@Nullable Collection<M> models) {
        if (models == null) throw new ValueIsNullException();

        for (final M model : models) {
            add(model);
        }
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models == null) throw new ValueIsNullException();

        removeAll();
        add(models);
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() {
       return repository.findAll();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable String id){
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        return (M)repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<M> models = findAll();
        return models.get(index);
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable M model = findOneById(id);
        if (model == null) return null;

        return removeOne(model);
    }

    @Nullable
    @Override
    @Transactional
    public M removeOneByIndex(@Nullable Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @Nullable M model = findOneByIndex(index);
        if (model == null) return null;

        return removeOne(model);
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

}