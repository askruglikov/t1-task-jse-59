package ru.t1.kruglikov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.user.UserLockRequest;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.event.ConsoleEvent;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class UserLockListener extends AbstractUserListener {

    @NotNull private final String NAME = "user-lock";
    @NotNull private final String DESCRIPTION = "User lock.";

    @Override
    @EventListener(condition = "@userLockListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOCK]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        userEndpoint.lock(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
