package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.enumerated.ProjectSort;
import ru.t1.kruglikov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    List<Project> findAll(
            @NotNull String userId,
            @NotNull ProjectSort sort
    );

    @Nullable
    List<Project> findAll(
            @NotNull ProjectSort sort
    );

}
