package ru.t1.kruglikov.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldExceprion {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
